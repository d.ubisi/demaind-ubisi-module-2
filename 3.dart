class App {
  String name = "Khula";
  String sector = "Agricultural Solution";
  String developer = "Karidas Tshintsholo and Matthew Piper";
  String year = "2018";

  void caps() {
    this.name = this.name.toUpperCase();
    print(this.name);
  }
}

void main(List<String> args) {
  App app = App();
  print(app.name);
  print(app.sector);
  print(app.developer);
  print(app.year);
  app.caps();
}
