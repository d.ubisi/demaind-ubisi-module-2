class Winnings {
  List<Map<int, String>> _winningAppsList = [
    {2021: "Ambani"},
    {2020: "Easy Equities"},
    {2019: "Naked Insurance"},
    {2018: "Khula"},
    {2017: "Standard Bank Shyft"},
    {2016: "Domestly"},
    {2015: "WumDrop"},
    {2014: "Live Inspect"},
    {2013: "SnapScan"},
    {2012: "FNB Banking"}
  ];

  getWinningApps() => _winningAppsList;
}

void main() {
  Winnings winnings = Winnings();

  var sorted = [];

  for (var i in winnings.getWinningApps()) {
    i.forEach((key, value) {
      sorted.add(value);
    });
  }
  sorted.sort();

  for (var i in sorted) {
    print(i);
  }

  print("\n");
  for (var i in winnings.getWinningApps()) {
    i.forEach((key, value) {
      if (key == 2017) {
        print('2017 winning app: $value');
      } else if (key == 2018) {
        print('2018 winning app: $value');
      }
    });
  }
  print("\n");
  print(winnings.getWinningApps().length);
}
