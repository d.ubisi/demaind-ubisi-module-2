class AboutMe {
  String _name = "Demaind";
  String _favoriteApp = "Twitter";
  String _city = "Tembisa";

  String name() => _name;

  String favoriteApp() => _favoriteApp;

  String city() => _city;
}

void main() {
  AboutMe aboutMe = AboutMe();
  print(aboutMe.name());
  print(aboutMe.favoriteApp());
  print(aboutMe.city());
}
